---
title: "Python: Как легко написать CLI инструмент для Linux с помощью Fire"
date: 2023-04-09
---
Хочу поделиться самым простым из известных мне способов написать CLI инструмент для администрирования Linux на Python.
<!--more-->
## Шаг 1. Установка Fire
```bash
pip install fire
```
## Шаг 2. Создаём простейший CLI инструмент
Вот пример CLI инструмента, который выводит в терминал версию Linux:
```python
#!/usr/bin/env python3
import fire
import platform


class SysInfo:
    """A CLI tool for getting system information about Linux server"""

    def kernel(self):
        """A method for getting kernel version"""
        version = platform.release()
        return f"Kernel version: {version}"


if __name__ == "__main__":
    obj = SysInfo()
    fire.Fire(obj)
```
Вставьте этот код в файл с именем `my-cli-tool` и дайте права на выполнение:
```bash
chmod +x my-cli-tool
```
Затем положите этот файл по пути `/usr/local/bin`:
```bash
sudo cp ./my-cli-tool /usr/local/bin
```

Чтобы воспользоваться этим инструментом, достаточно набрать команду:
```bash
my-cli-tool kernel
```

Вы увидите такой вывод:
```
❯ my-cli-tool kernel
Kernel version: 6.2.2-060202-generic
```

Как видите, достаточно создать класс, метод(ы) в нём, и передать объект класса внутрь функции fire.Fire() - и cli инструмент готов!
При этом автоматически сгенерируется help страница, вызвать которую можно с помощью флага `--help`:
```bash
my-cli-tool --help
```
Вы получите такой вывод:
```
NAME
    my-cli-tool - A CLI tool for getting system information about Linux server

SYNOPSIS
    my-cli-tool COMMAND

DESCRIPTION
    A CLI tool for getting system information about Linux server

COMMANDS
    COMMAND is one of the following:
      kernel
        A method for getting kernel version
```

## Шаг 3. Усложняем инструмент
Например, мы хотим также, чтобы наш инструмент мог выводить версию ядра в коротком варианте, то есть так: `6.2.2`.
Переписываем код следующим образом:
```python
#!/usr/bin/env python3
import fire
import platform


class SysInfo:
    """A CLI tool for getting system information about Linux server"""

    def kernel(self, format: ("short", "full") = "full"):
        """A method for getting kernel version"""
        version = platform.release()
        if format == "short":
            return version.split("-")[0]
        return f"Kernel version: {version}"


if __name__ == "__main__":
    obj = SysInfo()
    fire.Fire(obj)
```

Теперь мы можем набрать такую команду:
```bash
my-cli-tool kernel --format short
```
На что должен последовать такой вывод:
```
6.2.2
```
При этом автоматически будет скорректирована help страница, туда будет добавлен флаг `--format` и его возможные значения:
```bash
my-cli-tool kernel --help
```
Вывод:
```
NAME
    my-cli-tool kernel - A method for getting kernel version

SYNOPSIS
    my-cli-tool kernel <flags>

DESCRIPTION
    A method for getting kernel version

FLAGS
    -f, --format=FORMAT
        Type: ('short', 'full')
        Default: 'full'
```

## Шаг 4. Создаём бинарный файл

Сначала устанавливаем `pyinstaller`:
```bash
pip install pytinstaller
```
Затем запускаем команду:
```bash
pyinstaller my-cli-tool --onefile
```
У вас должна появиться папка `dist`, а в ней файл бинарный `my-cli-tool` со всеми зависимостями, который можно использовать даже на серверах,
на которых не установлен python или fire. Просто кладём этот файл по пути `/usr/local/bin` и `my-cli-tool` можно использовать!