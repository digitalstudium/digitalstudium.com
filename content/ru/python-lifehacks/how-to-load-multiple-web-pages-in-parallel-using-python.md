---
title: "Python: Как параллельно загрузить несколько веб-страниц"
date: 2022-05-15
---
В этой статье описывается, как загружать содержимое нескольких веб-страниц с нескольких URL-адресов параллельно с помощью Python.
<!--more-->
## Шаг 1. Установка aiohttp
Сначала нужно установить пакет `aiohttp`. Для установки aiohttp выполните команду:
```bash
pip install aiohttp[speedups]
```
Суффикс `[speedups]` нужен для установки ускоряющих aiohttp пакетов - `aiodns`, `cchardet`. 
## Шаг 1. Создание скрипта
Затем создайте файл main.py с таким кодом:
```python
import aiohttp
import asyncio
import socket


async def fetch_urls(urls):
	resolver = aiohttp.AsyncResolver()
	connector = aiohttp.TCPConnector(resolver=resolver, family=socket.AF_INET, use_dns_cache=False)
	session = aiohttp.ClientSession(connector=connector)

	async def fetch_url(url, session):
		async with session.get(url) as resp:
			print(resp.status)
			print(await resp.text())

	tasks = [fetch_url(url, session) for url in urls]
	await asyncio.gather(*tasks)
	await session.close()


loop = asyncio.get_event_loop()

urls = ['http://httpbin.org/get?key=value1', 'http://httpbin.org/get?key=value2', 'http://httpbin.org/get?key=value3']

loop.run_until_complete(fetch_urls(urls))								
```
Теперь можно запустить созданный файл с помощью команды:
```bash
python3 main.py
```
Вы увидите примерно такой вывод:
```
200
{
    "args": {
    "key": "value2"
    }, 
    "headers": {
    "Accept": "*/*", 
    "Accept-Encoding": "gzip, deflate", 
...	
```			
Все три запроса при этом выполнятся параллельно. Вы можете добавить любые url-ы в список <var>urls</var>, например:
```python
urls = ['https://yandex.com', 'https://google.com', 'https://yahoo.com']
```

Чтобы выполнить HEAD, POST, PUT, DELETE запросы, просто замените в коде `session.get(url)` на соответствующий
метод:

```python
session.post('http://httpbin.org/post', data=b'data')
session.put('http://httpbin.org/put', data=b'data')
session.delete('http://httpbin.org/delete')
session.head('http://httpbin.org/get')
session.options('http://httpbin.org/get')
session.patch('http://httpbin.org/patch', data=b'data')
```