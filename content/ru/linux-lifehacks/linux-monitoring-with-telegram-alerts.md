---
title: "Linux: Как настроить мониторинг с уведомлениями в Telegram"
date: 2023-03-04
---
В статье описывается, как настроить мониторинг с уведомлениями в Telergram с помощью Grafana, Prometheus, Alertmanager, Node-exporter и Cadvisor.
<!--more-->
### Первый шаг: Клонирование репозитория
Зайдите на сервер или в локальный терминал и выполните следующие команды:
```bash
git clone https://github.com/digitalstudium/grafana-docker-stack.git
cd grafana-docker-stack
git checkout alertmanager
```
### Второй шаг: установка внешнего адреса сервера
Откройте файл `docker-compose.yml` и в строках 22 и 38 измените адрес `127.0.0.1` на адрес того сервера, на котором вы хотите установить Prometheus.
### Третий шаг: создание бота в Telegram
В поиске Telegram наберите `Botfather` и перейдите по первой ссылке:

![Botfather поиск](/images/botfather1.png "Botfather поиск")

Затем нажмите `Start`:

![Botfather старт бота](/images/botfather2.png "Botfather старт бота")

После этого создайте бота, пользуясь инструкциями `Botfather`.
В итоге вы должны получить сообщение с токеном API:

![Botfather API токен](/images/botfather3.png "Botfather API токен")

Скопируйте данный токен и вставьте его в строку 14 файла `configs/alertmanager.yml` в качестве значения параметра `bot_token`.

Затем создайте группу в телеграм и добавьте созданного бота в эту группу. В эту группу будут приходить уведомления (алёрты).

### Четвертый шаг: Получение id группы
В группе, к которой вы добавили бота, напишите какую-нибудь команду, например: `/my_id foobar`

Затем в браузере перейдите по ссылке
`https://api.telegram.org/botINSERT_BOT_TOKEN_HERE/getUpdates`, заменив `INSERT_BOT_TOKEN_HERE` на токен, созданный на шаге 3.
Вы должны получить примерно такую страницу:

![Telegram получение id чата](/images/chat_id.png "Telegram получение id чата")
Если на вашей странице ничего нет, попробуйте отправьте команду `/my_id foobar` в группу ещё раз.

Вам нужно скопировать значение chat id с этой страницы и вставить его в строку 15 файла `configs/alertmanager.yml` в качестве значения параметра `chat_id`. Обратите внимание, что значение параметра `chat_id` должно быть без кавычек и со знаком дефиса вначале.

### Пятый шаг: развёртывание docker стэка
Находясь в папке `grafana-docker-stack`, выполните следующие команды:
```bash
docker swarm init
docker stack deploy -c docker-compose.yml monitoring
```
Подождите 5-10 минут, затем выполните команду
```bash
docker ps
```

Вывод команды должен содержать 5 контейнеров: prometheus, grafana, alertmanager, node-exporter, grafana.
```
CONTAINER ID   IMAGE                              COMMAND                  CREATED      STATUS                PORTS                       NAMES
46fba26e7234   gcr.io/cadvisor/cadvisor:v0.47.0   "/usr/bin/cadvisor -…"   5 days ago   Up 5 days (healthy)   8080/tcp                    monitoring_cadvisor.1.q02qcn798dh0rydo1dzslylse
f212e3c66786   prom/alertmanager:v0.25.0          "/bin/alertmanager -…"   6 days ago   Up 6 days             9093/tcp                    monitoring_alertmanager.1.oysziztrqnur7xr0hr82avunz
c16fb14929e2   prom/prometheus:v2.42.0            "/bin/prometheus --c…"   6 days ago   Up 6 days             9090/tcp                    monitoring_prometheus.1.yslspi4fgjp7ic4f5e18gm99a
9bf01ce6b7a1   grafana/grafana:9.3.6-ubuntu       "/run.sh"                6 days ago   Up 6 days             3000/tcp                    monitoring_grafana.1.mypn85x12xw37ucprr33knuwk
58efdb46f5c3   kindest/node:v1.25.3               "/usr/local/bin/entr…"   6 days ago   Up 6 days             127.0.0.1:46579->6443/tcp   kind-control-plane
ae15e453e517   prom/node-exporter:v1.5.0          "/bin/node_exporter …"   7 days ago   Up 7 days             9100/tcp                    monitoring_node-exporter.1.uecim10ow12h1qlpox5lg0c5r
```
Если вы видите такой вывод, значит всё развернулось успешно. Если нет, попробуйте повторить все предыдущие шаги.

### Шестой шаг: проверить работоспособность

Зайдите по адресу [ip-адрес или доменное имя сервера]:3000
У вас должна открыться Grafana. Введите логин `admin` и пароль `admin`. Grafana попросит сменить пароль, смените его на любой другой.
В разделе Dashboard -> Browse -> General вы увидите 2 дашборда: Cadvisor exporter и Node Exporter Full. Откройте их и убедитесь, что всё работает.

## Итог
Теперь вы можете оценивать производительность сервера через графики Grafana.
Также вам будут приходить уведомления о проблемах с сервером в группу Telegram. Правила уведомлений можно настроить в файле `configs/prometheus/alert_rules.yml`
