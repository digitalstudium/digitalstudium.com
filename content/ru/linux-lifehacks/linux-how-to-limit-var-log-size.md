---
title: "Linux: как ограничить размер папки /var/log"
date: 2022-06-06
---

Иногда папка `/var/log` увеличивается в размере настолько, что становится причиной нехватки места на диске. Как ограничить рост размера этой папки? Выполнив два шага из этой статьи, вы можете поставить размер папки `/var/log` под контроль.<!--more-->
### Шаг 1. Ограничение размера логов journald
Логи всех сервисов systemd складываются в папку `/var/log/journal/` сервисом `journald`. Чтобы установить предельный размер этой папки, выполните следующие команды:
```bash
sudo bash -c 'echo "SystemMaxUse=100M" >> /etc/systemd/journald.conf'
sudo systemctl restart systemd-journald
```
Вместо размера `100M` вы можете указать любой другой размер, в единицах измерения `K, M, G, T`. После вышеуказанных команд вы можете убедиться, что размер папки `/var/log` стал иметь заданный размер с помощью команды: `du -sh /var/log/journal/`
### Шаг 2. Ограничение количества файлов логов, ротируемых logrotate
Logrotate каждый день совершает ротацию почти всех лог-файлов, находящихся в папке `/var/log`. Например, если я наберу команду `ls /var/log/kern*`, то я увижу, что помимо файла `/var/log/kern.log` у меня хранится ещё 4 файла, которые сохранил logrotate:
```bash
ls /var/log/kern*
```
```
/var/log/kern.log    /var/log/kern.log.2.gz  /var/log/kern.log.4.gz
/var/log/kern.log.1  /var/log/kern.log.3.gz
```
Чтобы ограничить количество лог-файлов, нужно отредактировать файл `/etc/logrotate.d/rsyslog`. Посмотрев содержимое файла `/etc/logrotate.d/rsyslog`, мы увидим, что дефолтное значение параметра `rotate` равно `4`:
```bash
cat /etc/logrotate.d/rsyslog
```
```
/var/log/syslog
/var/log/mail.info
/var/log/mail.warn
/var/log/mail.err
/var/log/mail.log
/var/log/daemon.log
/var/log/kern.log
/var/log/auth.log
/var/log/user.log
/var/log/lpr.log
/var/log/cron.log
/var/log/debug
/var/log/messages
{
	rotate 4
	weekly
	missingok
	notifempty
	compress
	delaycompress
	sharedscripts
	postrotate
		/usr/lib/rsyslog/rsyslog-rotate
	endscript
}
```
Можно изменить `4` на какое-то другое значение, например, на `1`, чтобы хранился только один файл. В папке `/etc/logrotate.d/` вы найдёте также множество других конфигурационных файлов, относящихся к другим лог-файлам, там вы тоже можете изменить параметр `rotate`.