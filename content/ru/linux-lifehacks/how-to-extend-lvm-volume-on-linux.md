---
title: "Linux: Как расширить логический том LVM"
date: 2022-05-15
---
В этой статье описывается, как расширить группу LVM и том в операционной системе Linux.
<!--more-->
### Ситуация 1: новый диск
#### Первый шаг: создание физического тома

После того, как вы присоединили диск к физическому серверу или к виртуальной машине, вам нужно набрать команду:

```bash
sudo fdisk -l
```

Это нужно, чтобы убедиться, что диск распознан операционной системой, а также чтобы идентифицировать имя диска. Вывод команды будет примерно такой:

```
Disk /dev/vdc: 5 GiB, 5368709120 bytes, 10485760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes    
```

После того, как вы идентифицировали имя диска (в нашем случае это `/dev/vdc`), вы можете создать физический том с помощью команды:

```bash
sudo pvcreate /dev/vdc
```

Вы увидите такой вывод:

```
kostya@ubuntu-21-04:~$ sudo pvcreate /dev/vdc
Physical volume "/dev/vdc" successfully created.
kostya@ubuntu-21-04:~$    
```

#### Второй шаг: расширение группы томов

Чтобы увидеть список доступных групп томов, воспользуйтесь командой:

```bash
vgdisplay
```

Теперь можно расширить группу томов. Делается это такой командой:

```bash
sudo vgextend {vg-name} {pv-name}
```

В нашем случае это будет:

```bash
sudo vgextend vg-example /dev/vdc
```

Вы увидите такой вывод:

```
kostya@ubuntu-21-04:~$ sudo vgextend vg-example /dev/vdc
Physical volume "/dev/vdc" successfully created.
Volume group "vg-example" successfully extended
kostya@ubuntu-21-04:~$   
```

#### Третий шаг: расширение логического тома

Расширение логического тома делается такой командой:

```bash
sudo lvextend --size +{size} {vg-name/lv-name}
```

В нашем случае это будет:

```bash
sudo lvextend --size +2G vg-example/lv-example
```

Вы увидите такой вывод:

```
kostya@ubuntu-21-04:~$ sudo lvextend --size +2G vg-example/lv-example
Size of logical volume vg-example/lv-example changed from 5.00 GiB (1280 extents) to 7.00 GiB (1792 extents).
Logical volume vg-example/lv-example successfully resized.
kostya@ubuntu-21-04:~$  
```

Если же вы хотите, чтобы логический том использовал всё свободное место в группе томов, то наберите
команду:

```bash
sudo lvextend --extents +100%FREE vg-example/lv-example
```

#### Четвёртый шаг: расширение файловой системы

Если у вас файловая система `xfs`, то расширение делается такой командой:

```bash
sudo xfs_growfs /dev/{vg-name}/{lv-name}
```

В нашем случае это будет:

```bash
sudo xfs_growfs /dev/vg-example/lv-example
```

В случае с файловой системой ext4 замените команду `xfs_growfs` на `resize2fs`

### Ситуация 2: если изменился размер существующего диска

Иногда может измениться размер существующего диска, например, в случае с виртуальной машиной. В таком случае, первый шаг будет отличаться, второй шаг выполняться не будет, а остальные шаги будут такими же, как в ситуации с новым диском, описанной выше. На первом шаге нужно будет не создать физический том, а расширить существующий. Делается это такой командой:

```bash
sudo pvresize /dev/DISKNAME
```

Например,

```bash
sudo pvresize /dev/vdc
```