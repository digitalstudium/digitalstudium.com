---
title: "Linux: Как создать логический том LVM"
date: 2022-05-15
---
В этой статье описывается, как создать группу LVM и том для физического диска в операционной системе Linux.
<!--more-->
### Первый шаг: создание физического тома
После того, как вы присоединили диск к физическому серверу или к виртуальной машине, вам нужно набрать
команду:
```bash
sudo fdisk -l
```
чтобы убедиться, что диск распознан операционной системой, а также чтобы идентифицировать имя диска. Вывод
команды будет примерно такой:
```
Disk /dev/vdb: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
```
После того, как вы идентифицировали имя диска (в нашем случае это `/dev/vdb`), вы можете создать
физический том с помощью команды:
```bash
sudo pvcreate /dev/vdb
```
Вы увидите такой вывод:
```
kostya@ubuntu-21-04:~$ sudo pvcreate /dev/vdb
Physical volume "/dev/vdb" successfully created.
kostya@ubuntu-21-04:~$  
```
### Второй шаг: создание группы томов
Теперь нужно создать группу томов. Делается это такой командой:
```bash
sudo vgcreate {vgname} {pvname}
```
в нашем случае команда будет выглядеть так:
```bash
sudo vgcreate vg-example /dev/vdb
```
Вывод команды будет такой:
```
kostya@ubuntu-21-04:~$ sudo vgcreate vg-example /dev/vdb
Volume group "vg-example" successfully created
kostya@ubuntu-21-04:~$  
```
### Третий шаг: создание логического тома
Создание логического тома делается такой командой:
```bash
sudo lvcreate --size {size} --name {lv-name} {vg-name}
```
В нашем случае это будет:
```bash
sudo lvcreate --size 5G --name lv-example vg-example
```
Вы увидите такой вывод:
```
kostya@ubuntu-21-04:~$ sudo lvcreate --size 5G --name lv-example vg-example
Logical volume "lv-example" created.
kostya@ubuntu-21-04:~$   
```
Если же вы хотите, чтобы логический том использовал всё свободное место в группе томов, то наберите команду:
```bash
sudo lvcreate --extents 100%FREE --name lv-example vg-example
```
### Четвёртый шаг: создание файловой системы
Чтобы создать файловую систему xfs, наберите команду:
```bash
sudo mkfs.xfs /dev/vg-example/lv-example
```
Вывод команды будет такой:
```
kostya@ubuntu-21-04:~$ sudo mkfs.xfs /dev/vg-example/lv-example
meta-data=/dev/vg-example/lv-example isize=512    agcount=4, agsize=327680 blks
            =                       sectsz=512   attr=2, projid32bit=1
            =                       crc=1        finobt=1, sparse=1, rmapbt=0
            =                       reflink=1    bigtime=0
data     =                       bsize=4096   blocks=1310720, imaxpct=25
            =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
            =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
Discarding blocks...Done.
kostya@ubuntu-21-04:~$  
```
Для создания файловой системы ext4 замените команду mkfs.xfs на mkfs.ext4
### Пятый шаг: монтирование логического тома
Например, вы хотите смонтировать созданный логический том в папку `/opt`. В таком случае, добавьте такую
строку в файл `/etc/fstab`:
```bash
/dev/vg-example/lv-example  /opt xfs defaults 0 1
```
После этого наберите команду:
```bash
sudo mount -a
```
Убедиться в том, что логический том успешно смонтирован, вы можете с помощью команды
```bash
df -h /opt
```
Вывод должен быть такой:
```
kostya@ubuntu-21-04:~$ df -h /opt/
Filesystem                         Size  Used Avail Use% Mounted on
/dev/mapper/vg--random-lv--random  5.0G   68M  5.0G   2% /opt
kostya@ubuntu-21-04:~$      
```