---
title: "Nginx: примеры конфигурационных файлов"
date: 2023-03-26
---
Иногда возникает потребность быстро создать конфигурационный файл nginx: для хостинга статического вебсайта, для проксирования запросов к какому-либо серверу и т. п.

В данной статье приведены простые примеры конфигов Nginx для разных целей.

<!--more-->

Чтобы нижеприведенные конфиги заработали, нужно положить их по пути `/etc/nginx/conf.d/<some_name>.conf`, затем проверить
валидность конфига:

```bash
nginx -t
```

Затем выполнить команду для применения конфига:

```bash
sudo service nginx reload
```

## Пример 1: хостинг статического сайта.

```nginx
server {
  listen 80;
  server_name example.com www.example.com;

  access_log /var/log/nginx/example.com.log;

  location / {
    root /var/www/example.com;
    index index.html;
  }
}
```

## Пример 2: проксирующий сервер

```nginx
server {
  listen 80;
  server_name example.com www.example.com;

  access_log /var/log/nginx/example.com.log;

  location / {
    proxy_pass http://127.0.0.1:5000;
    proxy_buffering off;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-Port $server_port;
  }
}
```

## Пример 3: кэширующий прокси

В `/etc/nginx/nginx.conf` добавить:

```nginx
http {
  proxy_cache_path  /data/nginx/cache  levels=1:2    keys_zone=STATIC:10m;
  inactive=24h  max_size=1g;
}
```

В `/etc/nginx/conf.d` положить такой конфиг:

```nginx
server {
  listen 80;
  server_name example.com www.example.com;

  access_log /var/log/nginx/example.com.log;
  location / {
    proxy_pass http://1.2.3.4;
    proxy_set_header Host $host;
    proxy_cache STATIC;
    proxy_cache_valid 200 1d;
    proxy_cache_use_stale error timeout invalid_header updating http_500 http_502 http_503 http_504;
  }
}
```