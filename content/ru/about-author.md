---
title: Об авторе
image: /images/kostya.jpg
date: 2023-02-18
---
Меня зовут Константин Шуткин, я живу в городе Москва и я DevOps-инженер. С 2016 года я работал на этой должности в таких компаниях, как Северсталь, Сбербанк, Nvidia. Мой основной предмет исследований и работы это Linux, Docker, Kubernetes, программирование на Python и т. д., и я делюсь своими знаниями в этих областях на этом сайте и на ютуб канале.

Мой youtube канал: [https://youtube.com/DigitalStudium](https://youtube.com/DigitalStudium)

Мой Git: [https://git.digitalstudium.com](https://git.digitalstudium.com)

Мой email: [konstantin@shootkin.ru](mailto:konstantin@shootkin.ru)
