---
title: "BASH: arrays and hashmaps"
date: 2023-05-07
---
Sometimes there is a need to use in BASH such structures as lists (also known as arrays) and dictionaries (also known as hashmaps and associative arrays). In this post there are some samples how to work with them.
<!--more-->
## 1. Arrays

Array creation in bash can be done this way:

```bash
sample_array=(foo bar bazz)
```

In order to add single or multiple new elements to the end of array, you should use this syntax:

```bash
sample_array+=(six seven)
```

In order to get elements of the list in a cycle, you should use this syntax:

```bash
for i in ${sample_array[@]}
do
  echo $i
done
```

Here is an example how to get element by its index:

```bash
echo ${sample_array[0]}
echo ${sample_array[3]}

# 0, 3 etc. - elements' indexes
```

Array slicing:

```bash
sliced_array=${sample_array[@]:1} # will get all elements of a sample_array, starting with 1st
another_sliced_array=${sample_array[@]:1:5} # will get sample_array elements since 1st to 5th
```

## 2. Hashmaps
To create hashmap in bash use this syntax:

```bash
declare -A sample_hashmap=([one]=one [two]=two [three]=three [four]=four [five]=five)
```

This will add new key "foo" with value "bar":

```bash
sample_hashmap[foo]=bar
```

Cycle: 

```bash
for key in ${sample_hashmap[@]}
do
  echo ${sample_hashmap[$key]}
done
```