---
title: "Linux: how to limit /var/log folder size"
date: 2022-06-16
---

Sometimes the `/var/log` folder grows so large that it causes a shortage of disk space. How to limit the size of this folder? By following the two steps in this article, you can control the size of the `/var/log` folder.<!--more-->
### Step 1. Limiting the size of journald logs
The logs of all systemd services are added to the `/var/log/journal/` folder by the `journald` service. To set the size limit for this folder, run the following commands:
```bash
sudo bash -c 'echo "SystemMaxUse=100M" >> /etc/systemd/journald.conf'
sudo systemctl restart systemd-journald
```
Instead of the `100M` size, you can specify any other size, in `K, M, G, T` units. After the above commands, you can verify that the size of the folder `/var/log` has become the specified size using the command: 
```bash
du -sh /var/log/journal/
```
### Step 2. Limiting the number of log files stored by logrotate
Logrotate rotates almost all log files in the `/var/log` folder every day. For example, if I type the command `ls /var/log/kern*`, then I will see that in addition to the file `/var/log/kern.log` I have 4 more files stored that logrotate saved:
```bash
ls /var/log/kern*
```
```
/var/log/kern.log    /var/log/kern.log.2.gz  /var/log/kern.log.4.gz
/var/log/kern.log.1  /var/log/kern.log.3.gz
```

To limit the number of log files, edit the file `/etc/logrotate.d/rsyslog`. Looking at the contents of the `/etc/logrotate.d/rsyslog` file, we can see that the default value of the `rotate` parameter is `4`:
```bash
cat /etc/logrotate.d/rsyslog
```
```
/var/log/syslog
/var/log/mail.info
/var/log/mail.warn
/var/log/mail.err
/var/log/mail.log
/var/log/daemon.log
/var/log/kern.log
/var/log/auth.log
/var/log/user.log
/var/log/lpr.log
/var/log/cron.log
/var/log/debug
/var/log/messages
{
	rotate 4
	weekly
	missingok
	notifempty
	compress
	delaycompress
	sharedscripts
	postrotate
		/usr/lib/rsyslog/rsyslog-rotate
	endscript
}
```
You can change `4` to some other value, such as `1`, so that only one file is stored. In the `/etc/logrotate.d/` folder you will also find many other configuration files related to other log files, where you can also change the `rotate` setting.


