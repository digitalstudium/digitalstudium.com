---
title: "Linux: How to create LVM logical volume"
date: 2022-05-15
---
This article describes how to create an LVM group and volume for a physical disk in the Linux operating system.
<!--more-->
### First step: creating a physical volume
After you have attached the disk to a physical server or virtual machine, you need to type
this command:
```bash
sudo fdisk -l
```
to make sure the drive is recognized by the operating system, and to identify the drive name. Output
of command will be something like this:
```
Disk /dev/vdb: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes/512 bytes
I/O size (minimum/optimal): 512 bytes/512 bytes
```
Once you have identified the drive name (in our case it is `/dev/vdb`), you can create
physical volume using the command:
```bash
sudo pvcreate /dev/vdb
```
You will see output like this:
```
kostya@ubuntu-21-04:~$ sudo pvcreate /dev/vdb
Physical volume "/dev/vdb" successfully created.
kostya@ubuntu-21-04:~$
```
### Step two: create the volume group
Now we need to create a volume group. This is done by the following command:
```bash
sudo vgcreate {vgname} {pvname}
```
In our case, the command will look like this:
```bash
sudo vgcreate vg-example /dev/vdb
```
The command output will look like this:
```
kostya@ubuntu-21-04:~$ sudo vgcreate vg-example/dev/vdb
Volume group "vg-example" successfully created
kostya@ubuntu-21-04:~$
```
### Step three: creating the logical volume
Creating a logical volume can be done with the following command:
```bash
sudo lvcreate --size {size} --name {lv-name} {vg-name}
```
In our case, it will be:
```bash
sudo lvcreate --size 5G --name lv-example vg-example
```
You will see output like this:
```
kostya@ubuntu-21-04:~$ sudo lvcreate --size 5G --name lv-example vg-example
Logical volume "lv-example" created.
kostya@ubuntu-21-04:~$
```
If you want the logical volume to use all the free space in the volume group, then run the command:
```bash
sudo lvcreate --extents 100%FREE --name lv-example vg-example
```
### Fourth step: creating the filesystem
To create an xfs filesystem, type the command:
```bash
sudo mkfs.xfs /dev/vg-example/lv-example
```
The command output will look like this:
```
kostya@ubuntu-21-04:~$ sudo mkfs.xfs/dev/vg-example/lv-example
meta-data =/dev/vg-example/lv-example isize = 512 agcount = 4, agsize = 327680 blks
            = sectsz = 512 attr = 2, projid32bit = 1
            = crc = 1 finobt = 1, sparse = 1, rmapbt = 0
            = reflink = 1 bigtime = 0
data = bsize = 4096 blocks = 1310720, imaxpct = 25
            = sunit = 0 swidth = 0 blks
naming = version 2 bsize = 4096 ascii-ci = 0, ftype = 1
log = internal log bsize = 4096 blocks = 2560, version = 2
            = sectsz = 512 sunit = 0 blks, lazy-count = 1
realtime = none extsz = 4096 blocks = 0, rtextents = 0
Discarding blocks ... Done.
kostya@ubuntu-21-04:~$
```
To create an ext4 filesystem, replace the `mkfs.xfs` command with `mkfs.ext4`
### Step Five: Mount the Logical Volume
For example, suppose you want to mount the newly created logical volume to the `/opt` folder. In this
case, add this line to file `/etc/fstab`:
```bash
/dev/vg-example/lv-example /opt xfs defaults 0 1
```
After that, type the command:
```bash
sudo mount -a
```
You can verify that the logical volume has been mounted successfully using the command:
```bash
df -h /opt
```
The output should be like this:
```
kostya@ubuntu-21-04:~$ df -h /opt/
Filesystem                         Size  Used Avail Use% Mounted on
/dev/mapper/vg--random-lv--random  5.0G   68M  5.0G   2% /opt
kostya@ubuntu-21-04:~$
```