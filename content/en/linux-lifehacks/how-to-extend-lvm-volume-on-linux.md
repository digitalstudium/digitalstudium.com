---
title: "Linux: How to extend LVM volume"
date: 2022-05-15
---
This article describes how to expand an LVM group and volume on a Linux operating system.
<!--more-->
### Situation 1: new disk
#### First step: creating a physical volume

After you have attached the disk to a physical server or virtual machine, you need to type command:

```bash
sudo fdisk -l
```

This is to make sure the drive is recognized by the operating system, and to identify the drive name. Output of the command will be something like this:

```
Disk /dev/vdc: 5 GiB, 5368709120 bytes, 10485760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes/512 bytes
I/O size (minimum/optimal): 512 bytes/512 bytes
```

Once you have identified the drive name (in our case it is `/dev/vdc`), you can create physical volume using the command:

```bash
sudo pvcreate /dev/vdc
```

You will see output like this:

```
    kostya@ubuntu-21-04:~$ sudo pvcreate /dev/vdc
    Physical volume "/dev/vdc" successfully created.
    kostya@ubuntu-21-04:~$
```

#### Step two: extend the volume group

To get a list of available volume groups run this command:

```bash
vgdisplay
```

You can now extend the volume group. This is done by the following command:

```bash
sudo vgextend {vg-name} {pv-name}
```

In our case, it will be:

```bash
sudo vgextend vg-example /dev/vdc
```

You will see output like this:

```
kostya@ubuntu-21-04:~$ sudo vgextend vg-example /dev/vdc
Physical volume "/dev/vdc" successfully created.
Volume group "vg-example" successfully extended
kostya@ubuntu-21-04:~$
```

#### Step three: extending the logical volume

Extending a logical volume can be done with the following command:

```bash
sudo lvextend --size + {size} {vg-name/lv-name}
```

In our case, it will be:

```bash
sudo lvextend --size +2G vg-example/lv-example
```

You will see output like this:

```
kostya@ubuntu-21-04:~$ sudo lvextend --size +2G vg-example/lv-example
Size of logical volume vg-example/lv-example changed from 5.00 GiB (1280 extents) to 7.00 GiB (1792 extents).
Logical volume vg-example/lv-example successfully resized.
kostya@ubuntu-21-04:~$
```

If you want the logical volume to use all the free space in the volume group, then type
command:

```bash
sudo lvextend --extents +100%FREE vg-example/lv-example
```

#### Fourth step: extending the file system

If you have xfs file system, then the extending can be done with the following command:

```bash
sudo xfs_growfs /dev/{vg-name}/{lv-name}
```

In our case, it will be:

```bash
sudo xfs_growfs /dev/vg-example/lv-example
```

For ext4 filesystem, replace `xfs_growfs` with `resize2fs`

### Situation 2: if the size of the existing disk has changed

Sometimes the size of an existing disk can change, for example, in case of a virtual machine. In this case, the first step will be different, the second step will not be performed, and the rest of the steps will be pretty the same as in the situation with a new disc described above. The first step is not to create a physical volume, but to resize the existing one. It can be done with the command:

```bash
sudo pvresize /dev/DISKNAME
```

For example,

```bash
sudo pvresize /dev/vdc
```