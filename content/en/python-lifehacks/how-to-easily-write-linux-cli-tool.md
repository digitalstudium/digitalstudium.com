---
title: "Python: How to easily write a CLI tool for Linux using Fire"
date: 2023-04-09
---
I want to share the easiest way I know to write a CLI tool for Linux administration
using python and Fire.
<!--more-->
## Step 1: Install Fire
```bash
pip install fire
```
## Step 2. Create a simple CLI tool
Here is an example of a CLI tool that prints the Linux version to the terminal:
```python
#!/usr/bin/env python3
import fire
import platform


class SysInfo:
     """A CLI tool for getting system information about Linux server"""

     def kernel(self):
         """A method for getting kernel version"""
         version = platform.release()
         return f"Kernel version: {version}"


if __name__ == "__main__":
     obj = SysInfo()
     fire.Fire(obj)
```
Paste this code into a file called `my-cli-tool` and give it permission to execute:
```bash
chmod +x my-cli-tool
```
Then put this file in the path `/usr/local/bin`:
```bash
sudo cp ./my-cli-tool /usr/local/bin
```

To use this tool, just type the command:
```bash
my-cli-tool kernel
```

You will see output like this:
```
❯ my-cli-tool kernel
Kernel version: 6.2.2-060202-generic
```

As you can see, it is enough to create a class, a method(s) in it, and pass the class object to the fire.Fire() function - and the cli tool is ready!
This will automatically generate a help page, which can be called using the `--help` flag:
```bash
my-cli-tool --help
```
You will get this output:
```
NAME
     my-cli-tool - A CLI tool for getting system information about Linux server

SYNOPSIS
     my-cli-tool COMMAND

DESCRIPTION
     A CLI tool for getting system information about Linux server

COMMANDS
     COMMAND is one of the following:
       kernel
         A method for getting kernel version
```

## Step 3. Making the tool more complex
For example, we also want our tool to be able to print the kernel version in short form, like this: `6.2.2`.
We rewrite the code as follows:
```python
#!/usr/bin/env python3
import fire
import platform


class SysInfo:
     """A CLI tool for getting system information about Linux server"""

     def kernel(self, format: ("short", "full") = "full"):
         """A method for getting kernel version"""
         version = platform.release()
         if format == "short":
             return version.split("-")[0]
         return f"Kernel version: {version}"


if __name__ == "__main__":
     obj = SysInfo()
     fire.Fire(obj)
```

Now we can type the following command:
```bash
my-cli-tool kernel --format short
```
Output:
```
6.2.2
```
This will also automatically update the help page, adding the `--format` flag and its possible values:
```bash
my-cli-tool kernel --help
```
Output:
```
NAME
    my-cli-tool kernel - A method for getting kernel version

SYNOPSIS
    my-cli-tool kernel <flags>

DESCRIPTION
    A method for getting kernel version

FLAGS
    -f, --format=FORMAT
        Type: ('short', 'full')
        Default: 'full'
```

## Step 4. Create a binary file

First install `pyinstaller`:
```bash
pip install pytinstaller
```
Then we run the command:
```bash
pyinstaller my-cli-tool --onefile
```
A folder `dist` should appear, and a binary file `my-cli-tool` inside it with all dependencies, which can be used even on servers,
which do not have python or fire installed. Just put this file in the path `/usr/local/bin` and `my-cli-tool` can be used!