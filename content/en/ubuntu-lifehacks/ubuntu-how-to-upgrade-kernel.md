---
title: "Ubuntu: How to upgrade kernel"
date: 2022-05-14
---
This article describes how to upgrade Linux kernel on Ubuntu operating system via command line interface.
<!--more-->
### First method
The first method is very simple. We need to enter only one command in the terminal:
```bash
sudo apt update && sudo apt -y upgrade
```
The `sudo apt update` command will update the repository cache, and the `sudo apt -y upgrade` command will install new versions of all installed programs, including the linux kernel. The advantage of this method is that the latest version of the linux kernel, <i>officially supported</i> by Ubuntu OS, will be installed. The disadvantage of this method is that the <i>officially supported</i> kernel is usually not the newest. Sometimes it happens that it is necessary to install the latest version of the linux kernel. Real world example: your new laptop may have a CPU which is only supported in linux kernel version 5.12, while the officially supported version is older. And here the second method comes to the rescue.
### Second method
The first step is to go to https://kernel.ubuntu.com/~kernel-ppa/mainline/. On this site, you need to select the folder with the latest version of the linux kernel (at the very bottom of the page). Note that it is recommended to select the version without the "rc" suffix. The "rc" suffix means "release candidate", which in turn means that the given kernel version is not stable. On the page that opens, select the folder with the architecture of your processor. The architecture can be found using the `uname -p` command. If the output of this command is "x86_64", then select the amd64 folder. On the opened page there will be links to .deb files. We need to download 4 of them:
```
linux-headers-{version}-generic_{version}.{date}_amd64.deb
linux-headers-{version}_{version}.{date}_all.deb
linux-image-unsigned-{version}-generic_{version}.{date}_amd64.deb
linux-modules-{version}-generic_{version}.{date}_amd64.deb
```
After you have downloaded the files, you need to install them using the command:
```bash
sudo apt install -y ~/Downloads/linux-*.deb
```
Installation will take 1-5 minutes. After restarting the computer, the installed kernel version will be loaded.