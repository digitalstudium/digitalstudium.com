---
title: About author
image: /images/kostya.jpg
date: 2023-02-18
---
My name is Konstantin Shutkin, I am from Moscow, Russia and I am a DevOps engineer. Since 2016, I have worked in this position in companies such as Severstal, Sberbank, Nvidia. My main subject of research and work is Linux, Docker, Kubernetes, Python programming and more. I share my knowledge on this blog.

My Git: [https://git.digitalstudium.com](https://git.digitalstudium.com)

My email: [konstantin@shootkin.ru](mailto:konstantin@shootkin.ru)